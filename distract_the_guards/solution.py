import itertools
import math
from fractions import gcd


def factors(n):
    l = list(set(reduce(list.__add__,
                        ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0))))
    l.sort()
    return l


def groups(n):
    m = int(n / 2)
    cnt = 1
    while n >= 2:
        print cnt
        cnt *= len([i for i in itertools.combinations([x for x in range(n)], 2)])
        n -= 2
    cnt /= math.factorial(m)
    return cnt


answers = {}


def quick_pair(a, b):
    sm = (a + b) / gcd(a, b)
    return not (sm & (sm - 1)) == 0 and sm > 0


def test_pair(a, b, lst=False):
    global answers
 #   if (a + b) % 2 != 0 and x != y and (a + b) % 4 != 0:
 #       return True
    if (a, b) in answers:
        return answers if lst else True
    hist = []
    hist.append((a, b))
    hist.append((b, a))
    while True:
        smaller, larger = (a, b) if a < b else (b, a)
        gcf = gcd(smaller, larger)
        sm = larger + smaller
        print "{},{} - sum:{} sum/gcd:{} mod:{} factors:L {} H {} gcd:{} ".format(
            smaller, larger, sm, sm / gcf, larger % smaller, factors(smaller), factors(larger), gcf)
        if a == b:
            if not lst:
                return False
            for item in hist:
                answers[item] = False
            return answers
        if a > b:
            c = b
            b += b
            a -= c
        else:
            c = a
            a += a
            b -= c
        if (a, b) in hist:
            if not lst:
                return True
            for item in hist:
                answers[item] = True
            return answers
        hist.append((a, b))
        hist.append((b, a))


def merge_two_dicts(x, y):
    # z = x.copy()   # start with x's keys and values
    x.update(y)    # modifies z with y's keys and values & returns None
    # return z


import random
data = [9, 1, 3, 7, 48, 21, 59, 509, 121, 1027, 5]
data = [random.randint(1, 5) for i in range(1, 20)]
data = [5, 4, 4, 5, 4, 2, 2, 4, 3, 2, 2, 2, 2, 4, 3, 4, 4, 1, 5]
data = [5, 4, 4, 5, 4, 2, 2, 4, 3, 2, 2, 2, 2, 4, 3, 4, 4, 1, 5]
""" #data = [1, 7, 3, 21, 13, 19]
print datetime.datetime.now()
pairs = []
analsys = []
for x in range(1, 6):
    for y in range(1, 6):
        if ((x + y) % 4 == 0) and x != y:  # and test_pair(x, y) == False:
            # if test_pair(x,y) == False:
            # if (x+y) % 4 != 0 and x != y and test_pair(x,y) == False:
            s = "{}:{} sum({}) mod({}) gcd({}) - {} : {}".format(x, y, x + y, x %
                                                                 y if x > y else y % x, gcd(x, y), test_pair(x, y), quick_pair(x, y))
            analsys.append((x + y, s)) """

# Try to solve
def answer(banana_list):
    data = banana_list
    pair_lists = {}
    pair_lens = []
    used_nums = []
    unused_nums = []
    formed_pairs = set([])
    # 1 Get all posible pairs
    for i in range(len(data)):
        unused_nums.append(i)
        x = data[i]
        for j in range(len(data)):
            y = data[j]
            if quick_pair(x, y):
                if i in pair_lists:
                    if j not in pair_lists[i]:
                        pair_lists[i].append(j)
                else:
                    pair_lists[i] = []
                if j in pair_lists:
                    if i not in pair_lists[j]:
                        pair_lists[j].append(i)
                else:
                    pair_lists[j] = []
    for p in pair_lists:
        pair_lens.append((len(pair_lists[p]), p))
    print data
    print pair_lists
    print sorted(pair_lens)
    # 2 Innitial pair off
    for l in sorted(pair_lens):
        n = l[1]
        #print 'd{} : {}   n{} : {}'.format(data[n], [data[x] for x in pair_lists[n]], n, pair_lists[n])
        if n not in used_nums:
            smallest = None  # [0,100]
            for p in pair_lists[n]:
                #print "p{} {}".format(p,p not in used_nums)
                if p not in used_nums:
                    print "n:{} p:{}".format(data[n], data[p])
                    #print  smallest, '--', len(pair_lists[p])
                    if smallest is None or smallest[1] > len(pair_lists[p]):
                        smallest = [p, len(pair_lists[p])]
            #print 'in smallest', smallest
            if smallest is not None:
                pp = smallest[0]
                used_nums.append(pp)
                used_nums.append(n)
                unused_nums.remove(n)
                unused_nums.remove(pp)
                formed_pairs.add(frozenset([pp, n]))
            #print "used:{} unused:{}, pairs:{}".format(used_nums,unused_nums,formed_pairs)
    for pair in formed_pairs:
        print[data[x] for x in pair]
    print [data[x] for x in unused_nums]
    # 3 Itterative correction
    changed = True
    while changed:
        changed = False
        usd = used_nums[:]
        uusd = unused_nums[:]
        fp = formed_pairs.copy()
        # process
        for n in unused_nums:
            uusd.remove(n)
            sw2 = None
            pr = None
            np1 = None
            np2 = None
            for p in formed_pairs:
                p1 = [x for x in p]
                if quick_pair(data[n], data[p1[0]]):
                    for q in uusd:
                        if quick_pair(data[q], data[p1[1]]):
                            sw2 = q
                            pr = p
                            np1 = frozenset([n, p1[0]])
                            np2 = frozenset([q, p1[1]])
                            changed = True
                            break
                elif quick_pair(data[n], data[p1[1]]):
                    for q in uusd:
                        if quick_pair(data[q], data[p1[0]]):
                            sw2 = q
                            pr = p
                            np1 = frozenset([n, p1[1]])
                            np2 = frozenset([q, p1[0]])
                            changed = True
                            break
                if changed:
                    break
            if changed:
                fp.remove(pr)
                fp.add(np1)
                fp.add(np2)
                uusd.remove(sw2)
                break
        if changed:
            used_nums = usd
            unused_nums = uusd
            formed_pairs = fp
    for pair in formed_pairs:
        print[data[x] for x in pair]
    print [data[x] for x in unused_nums]
    return len(unused_nums)

print answer([1,2,1,1,1,1,2,1,1,1])
