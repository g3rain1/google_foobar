from decimal import Decimal, Context, getcontext

def answer(str_n):
    getcontext().prec = (len(str_n) + 1) * 2
    return sum1itr(long(str_n), Decimal(2).sqrt())

def sum1itr(N, a, acc = 0):
    while N > 0:
        #print N
        Np = long((a - 1) * N)
        acc = ((N + Np) * (N + Np + 1) / 2) - Np * (Np + 1) - acc
        N = Np
    return abs(acc)

def sum1(N, a):
    #print N
    if N > 0:
        Np = long((a - 1) * N)
        return ((N + Np) * (N + Np + 1) / 2) - Np * (Np + 1) - sum1(Np,a)
    else:
        return 0

def sum2itr(N, a, acc=0):
    s = 1
    while N > 0:

        #print N

        if a < 2:
            Np = long((a - 1) * N)
            acc = acc + s * ((N + Np) * (N + Np + 1) / 2)
            a = a / (a - 1)
            s*= -1
            N = Np
        else:
            acc = acc + s * N * (N + 1) / 2
            a = a - 1
    return abs(acc)

def sum2(N, a):
    #print N, a
    if N > 0:
        if a < 2:
            Np = long((a - 1) * N)
            return ((N + Np) * (N + Np + 1) / 2) - sum2(Np, a / (a - 1))
        else:
            return N * (N + 1) / 2 + sum2(N, a - 1)
    else:
        return 0

def compare(str_n, a):
    getcontext().prec = (len(str_n) + 1) * 10
    N = long(str_n)
    for i in range(1, N+1, N / 10L):
        sq = a
        print sum2itr(i, sq) - sum1itr(i, sq)
        

def answer__(n, a, step=1):
    getcontext().prec = (len(n) + 1 * 2)
    d = []
    n = long(n)
    sm = 0L
    sqrt2 = a
    for i in range(step, (n + 1), step):
        s = sqrt2 * i
        sq = long(s)
        sm += sq
        rm = (s) - sq
        #print "i:{}\t i*sqrt2:{} sq:{} rm:{} sm:{}".format(i, rm + sq, sq, rm, sm)
    return sm