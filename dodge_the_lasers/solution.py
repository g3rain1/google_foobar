from decimal import Decimal, Context, getcontext

def sum2itr(N, a, acc=0):
    s = 1
    while N > 0:
        #print N
        if a < 2:
            Np = long((a - 1) * N)
            acc = acc + s * ((N + Np) * (N + Np + 1) / 2)
            a = a / (a - 1)
            s*= -1
            N = Np
        else:
            acc = acc + s * N * (N + 1) / 2
            a = a - 1
    return abs(acc)

def sum2(N, a):
    #print N, a
    if N > 0:
        if a < 2:
            Np = long((a - 1) * N)
            return ((N + Np) * (N + Np + 1) / 2) - sum2(Np, a / (a - 1))
        else:
            return N * (N + 1) / 2 + sum2(N, a - 1)
    else:
        return 0

def answer(str_n):
    getcontext().prec = (len(str_n) + 1) * 2
    return sum2(long(str_n), Decimal(2).sqrt())

def range(start, stop, step=1):
    i = long(start)
    while i < stop:
        yield i
        i += step

def answer_(n, step=1):
    getcontext().prec = len(n)
    d = {}
    n = long(n)
    sm = 0
    rmsm = Decimal(0)
    sqrt2 = Decimal(2).sqrt()
    for i in range(step, (n + 1), step):
        s = sqrt2 * i
        sq = long(s)
        sm += sq
        rm = (s) - sq
        rmsm += rm
        ip = (n + 1) - i
        sp = sqrt2 * ip
        sqp = long(sqrt2 * ip)
        rmp = (sqrt2 * ip) - sqp
        if sq + sqp in d:
            d[sq + sqp].append(i)
        else:
            d[sq + sqp] = [i]
        if sq + sqp in d:
            d[sq + sqp].append(ip)
        else:
            d[sq + sqp] = [ip]
        print "i:{} ip:{}\t scomp:{}\t sq:{} sqp:{} sqcomp:{}\t rm:{} rmp:{} rcomp:{} sm:{}".format(
            i, ip, sp + s, sq, sqp, sq + sqp, rm, rmp, rmp + rm, sm)
    for i in d:
        print '{} : {}'.format(i, d[i])
    return sm


def answer__(n, a, step=1):
    getcontext().prec = len(n)
    d = []
    n = long(n)
    sm = 0
    rmsm = Decimal(0)
    sqrt2 = a
    for i in range(step, (n + 1), step):
        s = sqrt2 * i
        sq = long(s)
        sm += sq
        rm = (s) - sq
        rmsm += rm
        print "i:{}\t i*sqrt2:{} sq:{} rm:{} sm:{}".format(
            i, rm + sq, sq, rm, sm)
        d.append(sq)
    return sm





