#really Goole? No numpy. I just had to implement matrix math myself? Figuring out how to apply Absorbing Markov chains was not enough for you? Do you have all your developers reinvent the wheel
from fractions import Fraction

def identity_matrix(s):
    m = [None] * s
    for i in range(s):
        m[i] = [0] * s
        m[i][i] = 1
    return m

def subtract_matrix(mA, mB):
    m = [None] * len(mA)
    for i in range(len(mA)):
        m[i] = [None] * len(mA[i])
        for j in range(len(mA[i])):
            m[i][j] = mA[i][j] - mB[i][j]
    return m
def dot_product(a, b):
    r = 0
    for i in range(len(a)):
        r += (a[i] * b[i])
    return r
        
def multiply_matrix(mA, mB):
    m = [None] * len(mA)
    for i in range(len(mA)):
        m[i] = [None] * len(mB[0])
    print "mA mB"
    for row in m:
        print ','.join(str(item) for item in row)
    print "mA"#[2][2]
    for row in mA:
        print ','.join(str(item) for item in row)
    print "mB" #[2][4]
    for row in mB:
        print ','.join(str(item) for item in row)

    for r in range(len(mA)):
        for c in range(len(mB[0])):
            col = [mB[x][c] for x in range(len(mB))]
            dp = dot_product(mA[r], col)
            m[r][c] = dp
    return m            

def get_submatrix(m, w, h):
    mbl = []
    mbr = []
    mtl = []
    mtr = []
    for r in m[h:]:
        mbl.append(r[:w])
        mbr.append(r[w:])
    for r in m[:h]:
        mtl.append(r[:w])
        mtr.append(r[w:])
    return mbl, mbr, mtl, mtr

def arange_matrix(m, indxmp):
    M = [None] * len(m)
    for r in range(len(m)):
        row = m[r]
        R = [None] * len(m)
        for i in range(len(m)):
            R[indxmp[i]] = row[i]
        M[indxmp[r]] = R
    return M

def invert_matrix(m):
    n = len(m)
    table = [[0]*2*n for _ in xrange(n)]  
    for i in range(n):
        for j in range(n):
            table[i][j] = m[i][j]
        table[i][i+n] = 1      
    for i in range(n):          
        scalar = table[i][i]                                
        for j in range(2*n):
            table[i][j] /= scalar        
        for j in range(n):
            if j != i:
                scalar = table[j][i]
                for k in range(2*n):
                    table[j][k] -= scalar * table[i][k]
    m = [table[i][n:] for i in range(n)] 
    return m

def standard_form_matrix(m):
    inda = 0
    inds = len(m) - 1
    d = {}
    for r in range(len(m)):
        if sum(m[r]) == 0:
            m[r][r] = 1
            d[r] = inda
            print inda
            inda += 1
    inds = inda
    for r in range(len(m)):
        if not(sum(m[r]) == 1 and m[r][r] == 1):
            d[r] = inds
            print inds
            inds += 1
    print inda
    print inds
    m = arange_matrix(m, d)
    R, Q, I, O = get_submatrix(m, inda, inda)
    return m, R, Q, I, O, d

def fractionize_matrix(m):
    for r in range(0, len(m)):
        print m[r]
        s = sum(m[r])
        for c in range(0, len(m[r])):
            if m[r][c] != 0:
                m[r][c] = Fraction(m[r][c], s)
            else:
                m[r][c] = Fraction(0,1)
    return m
def gcd(a,b):
    while b > 0:
        a, b = b, a % b
    return a
    
def lcm(a, b):
    return a * b / gcd(a, b)
    
def answer(m):
    for row in m:
        print ','.join(str(item) for item in row)
    print '\r'
    m, R, Q, I, O, arangement = (standard_form_matrix(fractionize_matrix(m)))
    print arangement
    print 'R'
    for row in R:
        print ','.join(str(item) for item in row)
    print 'Q'
    for row in Q:
        print ','.join(str(item) for item in row)
    print 'IQ'
    IQ = subtract_matrix(identity_matrix(len(Q)), Q)
    for row in IQ:
        print  ','.join(str(item) for item in row)
    F = invert_matrix(IQ)
    print 'F'
    for row in F:
        print ','.join(str(item) for item in row)
    print 'FR'
    FR = multiply_matrix(F, R)
    for row in FR:
        print ','.join(str(item) for item in row)
    if len(FR) == 0:
        return[1,1]
    probabilities = FR[0]   
    comdenom = reduce(lcm, [fract.denominator for fract in probabilities])
    for i in range(len(probabilities)):
        probabilities[i] = probabilities[i].numerator * (comdenom / probabilities[i].denominator)
    probabilities.append(comdenom)
    return probabilities
