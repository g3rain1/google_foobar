def answer(s):
    hall_length = len(s)
    hall = s.replace('-', '0')
    left = hall.replace('<', '1').replace('>', '0')
    right = hall.replace('>', '1').replace('<', '0')
    #convert binary strings to ints
    left = int(left, 2)
    right = int(right, 2)
    passes = 0
    for i in range(1, hall_length):
        p = left & (right >> i)#an int who's number of high bits represents the number of passes this step
        p = bin(p).count("1") #convert to string and count the 1's
        passes = passes + p 
    return int(passes * 2)