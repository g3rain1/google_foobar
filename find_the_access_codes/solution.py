def answer_nieve(l):
    ##l.sort()
    L = len(l)
    count = 0
    for i in range(L-2):
        for j in range(i + 1, L - 1):
            if l[j] % l[i] == 0:
                for k in range(j + 1, L):
                    if l[k] % l[j] == 0:
                        count += 1
    return count

def answer_split(l):
    ##l.sort()
    L = len(l)
    count = 0
    for y in range(L - 1):
        xy = len([x for x in l[:y] if l[y] % x == 0])
        yz = len([z for z in l[y + 1:] if z % l[y] == 0])
        count += xy * yz
    return count
        

def answer_store_pairs(l):
    ##l.sort()
    L = len(l)
    count = 0
    pair_counts = {}
    pair_ends = []
    for i in range(L - 1):
        for j in range(i + 1, L):
            if l[j] % l[i] == 0:
                pair_ends.append(j)
                pair_counts[i] = pair_counts.get(i, 0) + 1
                
    for k in pair_ends:
        count += pair_counts.get(k,0)
    return count

def answer(l):
    return answer_store_pairs(l)