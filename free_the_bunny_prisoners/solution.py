import itertools

def x_choose_y(x, y):
    return len([x for x in itertools.combinations([i for i in range(x)], y)])

def probspc(buns, required):
    recur = (buns + 1) - required
    max_key = x_choose_y(buns, recur)
    num_keys = max_key * recur
    columns = num_keys / buns
    return max_key - 1, columns, recur


def gen_matrix(buns, max, cols, rec):
    matrix = []
    for i in range(buns):
        matrix.append([])
        for j in range(cols):
            matrix[i].append(None)
    for i in range(cols):
        matrix[0][i] = i
    for i in range(rec):
        matrix[i][0] = 0
    for i in range(rec, buns):
        matrix[i][0] = i - rec + 1
    for i in range(buns - rec, buns):
        matrix[i][cols - 1] = max
    return matrix

def check_all_unique(li):
    m = "asd{}"
    li.sort()
    for i in range(1,len(li)):
       if li[i - 1] == li[i]: return False
    return True


# constraints
#r, y <= r + 1, y
#r, y <= r, y + 1
#use each # recur times
#use each # once per row
def solve_matrix(matrix, unassigned, domain, answers):
    #print '------\n', domain
    #print unassigned
    #for rw in matrix:
    #    print rw
    m_len = len(matrix)
    m_wid = len(matrix[0])
    m = max(domain) + 1
    for col in range(m_wid):
        for row in range(m_len):
            if matrix[row][col] is None:
                for d in domain:
                    #row = pair[0]
                    #col = pair[1]
                    center = d
                    up = matrix[row-1][col] if row - 1 >= 0 else m
                    down = matrix[row + 1][col] if row + 1 < len(matrix) else None
                    right = matrix[row][col + 1] if col + 1 < len(matrix[row]) else None
                    left = matrix[row][col - 1] if col - 1 >= 0 else m
                    if (center > left and left is not None) and (center >= up and up is not None) and (center <= down or down is None) and (center < right or right is None):
                        uc = unassigned[:]
                        dc = domain.copy()
                        mc = [r[:] for r in matrix]  # deepcopy(matrix)
                        mc[row][col] = d
                        dc[d] -= 1
                        if dc[d] == 0:
                            dc.pop(d)
                        #del dc[dc.index(d)]
                        #del uc[uc.index(pair)]
                        if len(dc) == 0:
                            if check_all_unique(mc) and mc not in answers:
                                answers.append(mc)
                            return
                        solve_matrix(mc, uc, dc, answers)
    return answers

#works but is slow
def answer2(num_buns, num_required):
    max, cols, rec = probspc(num_buns, num_required)
    innitial_matrix = gen_matrix(num_buns, max, cols, rec)
    unassigned_ind = []
    #assigned_vars = []
    domain = {}
    for i in range(num_buns):
        for j in range(cols):
            if innitial_matrix[i][j] is None:
                unassigned_ind.append((i, j))

    for i in range(max + 1):
        domain[i] = rec
    for row in innitial_matrix:
        #print row
        for e in row:
            if e in domain:
                domain[e] -= 1
                if domain[e] == 0:
                    domain.pop(e)
    if len(unassigned_ind) == 0:
        return innitial_matrix
    ans = solve_matrix(innitial_matrix, unassigned_ind, domain, [])
    lw = ans[0]
    for a in ans:
        if a < lw:
            lw = a
    return lw

def answer(num_buns, num_required):

    max, cols, rec = probspc(num_buns, num_required)
    matrix = []
    for i in range(num_buns):
        matrix.append([])
    if num_required == 0 or num_required > num_buns:
        return matrix
    row_nums = [x for x in reversed(sorted(set([x for x in itertools.permutations([True] * rec + [False] * (num_buns - rec))])))]
    for x in range(len(row_nums)):
        for i in range(len(row_nums[x])):
            if row_nums[x][i]:                
                matrix[i].append(x)
    return matrix
