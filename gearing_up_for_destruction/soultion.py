from fractions import Fraction  
def answer(pegs):
    if pegs is None or len(pegs) <= 1:
        return [-1,-1]
    nPegs = len(pegs)    
    if nPegs % 2 == 0:
        sum = - pegs[0] + pegs[nPegs - 1]
        if nPegs > 2:
            for i in xrange(1, nPegs-1):
                sum += 2 * (-1)**(i+1) * pegs[i]
        radius = Fraction(2 * (float(sum)/3 )).limit_denominator()
    else:
        sum =  - pegs[0] - pegs[nPegs -1]
        if nPegs > 2:
            for i in xrange(1, nPegs-1):
                sum += 2 * (-1)**(i+1) * pegs[i]
        radius = Fraction(2 * sum).limit_denominator()

    radiusCheck = radius
    for i in range(0, nPegs-2):
        distanceRemaining = pegs[i+1] - pegs[i]
        raidusCheckN1 = distanceRemaining - radiusCheck
        if (radiusCheck < 1 or raidusCheckN1 < 1):
            return [-1,-1]
        else:
            radiusCheck = raidusCheckN1

    return [radius.numerator, radius.denominator]