import base64

MESSAGE = '''
PBQBFAoNVDRAVUFTThYgQRcAHUkdZxQRDgUCVCZUBwROTgtnFBcSHQtUKlYWRkVOFiJVFA4bGkJg E0hBTgdfJEEXBQAMXSIUXkFOD1IvWhcXDANUKUdVQVNOFjJdHg4KBVQjFF5BThxQJVEbFRpJEX0T VRIICFRgH1JGDwFeYBNIQU4ZWCkSVRw=
'''

def message(KEY):
    result = [] 
    for i, c in enumerate(base64.b64decode(MESSAGE)):
        result.append(chr(ord(c) ^ ord(KEY[i % len(KEY)])))

    print ''.join(result)