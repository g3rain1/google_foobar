def removal_pattern(n):
    for num_ones in range(0, n+1):
        # init list
        lst = [i for i in reversed(range(0, num_ones))]
        orig = lst[:]
        setcomp = False
        while(not setcomp):
            yield lst
            setcomp = True
            i = num_ones - 1
            while(i >= 0):
                if i < len(lst):
                    if (lst[i-1] > lst[i] + 1 or i == 0) and lst[i] + 1 < n:
                        lst[i] += 1
                        lst[i + 1:] = orig[i + 1:]
                        setcomp = False
                        i = num_ones - 1
                        break
                i -= 1

def answer(L):
    for l in removal_pattern(len(L)):
        sub = L[:]
        sub.sort()
        for ind in l:
            sub[ind] = -1
        sub = [y for y in sub if y != -1]
        if sum(sub) % 3 == 0 and len(sub) > 0 and sum(sub) != 0:
            return int(''.join(str(s) for s in reversed(sub)))
    return 0