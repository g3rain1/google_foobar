cache = {}
def steps(n, p, first=False):
    arg = (n, p, first)
    global cache
    if arg in cache:
        return cache[arg]
    #print "steps({})".format(n)
    if first:
        if n == 3:
            cache[arg] = 1
            return 1
        if n < 3:
            cache[arg] = 0
            return 0
    elif not first:
        if n == 0:  
            cache[arg] = 1
            return 1
    high = n -1 if first else n
    low = 0
    
    combinations = 0
    for t in reversed(range(low, high + 1)):
        if t < p:            
            combinations += steps(n - t, t)
    cache[arg] = combinations
    return combinations


def answer(n):
    return steps(n, n, first=True)

for i in range(3, 201):
    print "{} : {}".format(i, answer(i))